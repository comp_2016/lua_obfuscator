package obfuscator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class RandomString {
	static final Random rnd = new Random();
	static final String randStr = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWYZ0123456789";
	ArrayList<ArrayList<String>> comments = new ArrayList<ArrayList<String>>();
		
	public String generateName() {
		String name = "";
		for (int i = 0; i < 3 + rnd.nextInt(7); i++)
			name += randStr.charAt(rnd.nextInt(randStr.length()));
		
		return name;
	}
	
	public ArrayList<String> generateComment() {
		if (comments.size() == 0)
			loadComments();
		
		return comments.get(rnd.nextInt(comments.size()));
	}
	
	private void loadComments() {
		comments.add(new ArrayList<String>(Arrays.asList(
				"When I wrote this, only God and I understood what I was doing",
				"Now, God only knows")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"stop() -- Hammertime!")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"sometimes I believe compiler ignores all my comments")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"I dedicate all this code, all my work, to my wife, Darlene, who will ",
				"have to support me and our three children and the dog once it gets ",
				"released into the public.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"drunk, fix later")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"Magic. Do not touch.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"define TRUE FALSE")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"Happy debugging suckers")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"I'm sorry.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"return 1 -- returns 1")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"You may think you know what the following code does.",
				"But you dont. Trust me.",
				"Fiddle with it, and youll spend many a sleepless",
				"night cursing the moment you thought youd be clever",
				"enough to \"optimize\" the code below.",
				"Now close this file and go play with something else.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"This code sucks, you know it and I know it.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"Move on and call me an idiot later.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"double penetration",
				"ouch")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"-------------------------this is a well commented line")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"I don't know why I need this, but it stops the people being upside-down",
				"x = -x")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"I am not sure if we need this, but too scared to delete.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"To understand recursion, see the line below",
				"To understand recursion, see the previous line")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"I am not responsible of this code.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"They made me write it, against my will.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"I did this the other way")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"Please work")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"no comments for you")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"it was hard to write",
				"so it should be hard to read")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"I have to find a better job")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"code below replaces code above - any problems?",
				"yeah, it doesn't fucking work.")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"function isDirty()",
				"--Why do you always go out and",
				"return dirty",
				"end")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"repeat",
				"   ...",
				"until (JesusChristsReturn) -- not sure")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"If this code works, it was written by Paul DiLascia. If not, I don't know",
				"who wrote it")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"this formula is right, work out the math yourself if you don't believe me")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"Comment this later")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"Remove this if you wanna be fired")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"Peter wrote this, nobody knows what it does, don't change it!")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"Are you kidding me??")));
		comments.add(new ArrayList<String>(Arrays.asList(
				"¯\\_(ツ)_/¯")));
	}

}
