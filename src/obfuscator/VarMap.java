package obfuscator;

import java.util.HashMap;
import java.util.Map;

public class VarMap {
	
	public static Map<String, String> variables = new HashMap<String, String>();
	
	public String get(String id) {
		return variables.get(id);		
	}
	
	public String set(String id, String alias) {
		return variables.put(id, alias);
	}
	
}
