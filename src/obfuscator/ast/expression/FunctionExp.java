package obfuscator.ast.expression;

import obfuscator.ast.expression.Exp;
import obfuscator.ast.statement.FuncBody;
import obfuscator.VarMap;

public class FunctionExp extends Exp{
	
	public final FuncBody fb;
	
	public FunctionExp(FuncBody fb) {
		this.fb = fb;
	}

	@Override
	public String unparse() {
		return "function " + fb.unparse();
	}

	@Override
	public Exp obfuscate() {
		return new FunctionExp(fb.obfuscate());
	}
	
	@Override
	public void getVariables(VarMap varMap) {
		fb.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		FuncBody funcBody = (FuncBody) fb.renameVariables(varMap);
		return new FunctionExp(funcBody);
	}

}
