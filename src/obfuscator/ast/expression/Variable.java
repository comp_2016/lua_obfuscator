package obfuscator.ast.expression;

import obfuscator.RandomString;
import obfuscator.VarMap;

public class Variable extends Exp {
	
	public final String id;
	public final Exp preExp;
	public final Exp exp;
	public final String name;
	
	public Variable(String name) {
		this.id = null;
		this.preExp = null;
		this.exp = null;
		this.name = name;
	}

	public Variable(String id, Exp preExp, Exp exp, String name) {
		this.id = id;
		this.preExp = preExp;
		this.exp = exp;
		this.name = name;
	}

	@Override
	public String unparse() {
		if (id != null && preExp == null && exp == null)
			return id;
		else if (id == null && name == null)
			return preExp.unparse() + "[" + exp.unparse() + "]";
		else if (id == null && exp == null)
			return preExp.unparse() + "." + name;
		return "";
	}

	@Override
	public Exp obfuscate() {
		String obfId = id;
		Exp obfPreExp = preExp;
		Exp obfExp = exp;
		String obfName = name;
		
		if(preExp != null) 
			obfPreExp = preExp.obfuscate();
		
		if(exp != null)
			obfExp = exp.obfuscate();
		
		return new Variable(obfId, obfPreExp, obfExp, obfName);
	}

	@Override
	public void getVariables(VarMap varMap) {
		RandomString rndStr = new RandomString();
		
		if (id != null && VarMap.variables.get(id) == null) {
			String alias = rndStr.generateName();
			while (VarMap.variables.containsValue(alias))
				alias = rndStr.generateName();
			VarMap.variables.put(name, alias);
		}
		
		if (preExp != null) 
			preExp.getVariables(varMap);
		
		if (exp != null) 
			exp.getVariables(varMap);
		
		if (name != null && VarMap.variables.get(name) == null) {
			String alias = rndStr.generateName();
			while (VarMap.variables.containsValue(alias))
				alias = rndStr.generateName();
			VarMap.variables.put(name, alias);
		}
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		String _id = id;
		Exp pe = preExp;
		Exp e = exp;
		String n = name;
		
		if (id != null && VarMap.variables.get(id) != null)
			_id = VarMap.variables.get(id);

		if (preExp != null) 
			pe = preExp.renameVariables(varMap);
		
		if (exp != null) 
			e = exp.renameVariables(varMap);
		
		if (name != null && VarMap.variables.get(name) != null)
			n = VarMap.variables.get(this.name);

		return new Variable(_id, pe, e, n);
	}

}
