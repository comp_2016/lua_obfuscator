package obfuscator.ast.expression;

import java.util.ArrayList;
import obfuscator.ast.expression.Exp;
import obfuscator.VarMap;

public class FieldList extends Exp {

	public final ArrayList<Field> fields;
	public String fs;
	public Boolean lefs; // Last entry field separator
	
	public void setFs(String fs) {
		this.fs = fs;
	}

	public FieldList(ArrayList<Field> fields, String fs, Boolean lefs){
		this.fields = fields;
		this.fs = fs;
		this.lefs = lefs;
	}
	
	public void add(Field field){
		if (fields != null)
			fields.add(field);
	}
	
	@Override
	public String unparse() {
		String fieldList = "";
		
		for(int i = 0; i < fields.size()-1; i++) {
			fieldList += fields.get(i).unparse() + fs;
		}
		fieldList += fields.get(fields.size()-1) + (lefs? fs : "");
		
		return fieldList;
	}

	@Override
	public Exp obfuscate() {
		ArrayList<Field> list = new ArrayList<Field>();
		
		for (Field field : this.fields)
			list.add((Field) field.obfuscate());
		
		return new FieldList(list, fs, lefs);
	}

	@Override
	public void getVariables(VarMap varMap) {
		for (Field field : fields)
			field.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		ArrayList<Field> list = new ArrayList<Field>();
		
		for (Field field : fields)
			list.add((Field) field.renameVariables(varMap));
		
		return new FieldList(list, fs, lefs);
	}

}
