package obfuscator.ast.expression;

import obfuscator.ast.expression.Exp;
import obfuscator.VarMap;

public class Nil extends Exp{
	
	public Nil() {
		// NOOP
	}
	
	@Override
	public String unparse() {
		return "nil";
	}
	
	@Override
	public Exp obfuscate() {
		return this;
	}
	
	@Override
	public void getVariables(VarMap varMap) {
		// NOOP
	}
	
	@Override
	public Exp renameVariables(VarMap varMap) {
		return this;	
	}

}
