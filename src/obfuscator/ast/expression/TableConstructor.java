package obfuscator.ast.expression;

import obfuscator.ast.expression.Exp;
import obfuscator.VarMap;

public class TableConstructor extends Exp {

	public final FieldList fieldlist;
	
	public TableConstructor(FieldList fieldlist){
		this.fieldlist= fieldlist;
	}
	
	@Override
	public String unparse() {
		return "{" + fieldlist.unparse() + "}";
	}

	@Override
	public Exp obfuscate() {
		return new TableConstructor((FieldList) fieldlist.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		fieldlist.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new TableConstructor((FieldList) fieldlist.renameVariables(varMap));
	}

}
