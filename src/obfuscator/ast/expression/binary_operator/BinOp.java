package obfuscator.ast.expression.binary_operator;

import obfuscator.ast.expression.Exp;

/**
 * Abstract representation of Lua's binary operators.
 */
public abstract class BinOp extends Exp {

}
