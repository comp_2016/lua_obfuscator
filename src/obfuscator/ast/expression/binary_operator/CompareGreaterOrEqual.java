package obfuscator.ast.expression.binary_operator;

import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class CompareGreaterOrEqual extends BinOp {

	public final Exp left;
	public final Exp right;

	public CompareGreaterOrEqual(Exp left, Exp right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String unparse() {
		return "(" + left.unparse() + " >= " + right.unparse() + ")";
	}

	@Override
	public String toString() {
		return "CompareGreaterOrEqual(" + left + ", " + right + ")";
	}

	@Override
	public Exp obfuscate() {
		Exp obfuscatedLeft = left.obfuscate();
		Exp obfuscatedRight = right.obfuscate();
		return new CompareLess(obfuscatedLeft, obfuscatedRight);
	}

	@Override
	public void getVariables(VarMap varMap) {
		left.getVariables(varMap);
		right.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new CompareGreaterOrEqual(left.renameVariables(varMap), right.renameVariables(varMap));
	}

}
