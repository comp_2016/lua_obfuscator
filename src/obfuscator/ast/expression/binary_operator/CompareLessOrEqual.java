package obfuscator.ast.expression.binary_operator;

import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class CompareLessOrEqual extends BinOp {

	public final Exp left;
	public final Exp right;

	public CompareLessOrEqual(Exp left, Exp right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String unparse() {
		return "(" + left.unparse() + " <= " + right.unparse() + ")";
	}

	@Override
	public String toString() {
		return "CompareLessOrEqual(" + left + ", " + right + ")";
	}

	@Override
	public Exp obfuscate() {
		Exp obfuscatedLeft = left.obfuscate();
		Exp obfuscatedRight = right.obfuscate();
		return new CompareGreater(obfuscatedLeft, obfuscatedRight);
	}

	@Override
	public void getVariables(VarMap varMap) {
		left.getVariables(varMap);
		right.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new CompareLessOrEqual(left.renameVariables(varMap), right.renameVariables(varMap));
	}

}
