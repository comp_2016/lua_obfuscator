package obfuscator.ast.expression.binary_operator;

import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class CompareGreater extends BinOp {

	public final Exp left;
	public final Exp right;

	public CompareGreater(Exp left, Exp right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String unparse() {
		return "(" + left.unparse() + " > " + right.unparse() + ")";
	}

	@Override
	public String toString() {
		return "CompareGreater(" + left + ", " + right + ")";
	}

	@Override
	public Exp obfuscate() {
		return new CompareGreater(left.obfuscate(), right.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		left.getVariables(varMap);
		right.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new CompareGreater(left.renameVariables(varMap), right.renameVariables(varMap));
	}

}
