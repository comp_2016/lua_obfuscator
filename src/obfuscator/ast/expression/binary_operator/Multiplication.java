package obfuscator.ast.expression.binary_operator;

import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class Multiplication extends BinOp {
	
	public final Exp left;
	public final Exp right;

	public Multiplication(Exp left, Exp right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String unparse() {
		return "(" + left.unparse() + " * " + right.unparse() + ")";
	}

	@Override
	public Exp obfuscate() {
		return new Multiplication(left.obfuscate(), right.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		left.getVariables(varMap);
		right.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new Multiplication(left.renameVariables(varMap), right.renameVariables(varMap));
	}

}
