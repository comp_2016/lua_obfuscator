package obfuscator.ast.expression.binary_operator;

import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class Division extends BinOp {

	public final Exp left;
	public final Exp right;

	public Division(Exp left, Exp right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String unparse() {
		return "(" + left.unparse() + " / " + right.unparse() + ")";
	}

	@Override
	public Exp obfuscate() {
		return new Division(this.left.obfuscate(), this.right.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		left.getVariables(varMap);
		right.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new Division(left.renameVariables(varMap), right.renameVariables(varMap));
	}
}
