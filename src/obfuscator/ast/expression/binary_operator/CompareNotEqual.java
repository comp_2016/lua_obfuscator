package obfuscator.ast.expression.binary_operator;

import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class CompareNotEqual extends BinOp {
	
	public final Exp left;
	public final Exp right;

	public CompareNotEqual(Exp left, Exp right) {
		this.left = left;
		this.right = right;
	}

	@Override
	public String unparse() {
		return "(" + left.unparse() + " ~= " + right.unparse() + ")";
	}

	@Override
	public Exp obfuscate() {
		return new CompareNotEqual(left.obfuscate(), right.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		left.getVariables(varMap);
		right.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new CompareNotEqual(left.renameVariables(varMap), right.renameVariables(varMap));
	}

}
