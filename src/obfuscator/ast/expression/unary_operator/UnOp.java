package obfuscator.ast.expression.unary_operator;

import obfuscator.ast.expression.Exp;

/**
 * Abstract representation of Lua's unary operators.
 */
public abstract class UnOp extends Exp {

}
