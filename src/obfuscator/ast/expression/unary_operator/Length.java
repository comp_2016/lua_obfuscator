package obfuscator.ast.expression.unary_operator;

import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class Length extends UnOp {
	
	public final Exp exp;

	public Length(Exp exp) {
		this.exp = exp;
	}

	@Override
	public String unparse() {
		return "# " + exp.unparse();
	}

	@Override
	public Exp obfuscate() {
		return new Length(this.exp.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		exp.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new Length(exp.renameVariables(varMap));
	}
	
}
