package obfuscator.ast.expression.unary_operator;

import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class Not extends UnOp {
	
	public final Exp exp;

	public Not(Exp exp) {
		this.exp = exp;
	}

	@Override
	public String unparse() {
		return "(not " + exp.unparse() + ")";
	}

	@Override
	public Exp obfuscate() {
		return new Not(exp.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		exp.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new Not(exp.renameVariables(varMap));
	}

}
