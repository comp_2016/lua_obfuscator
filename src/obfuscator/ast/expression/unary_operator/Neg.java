package obfuscator.ast.expression.unary_operator;

import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class Neg extends UnOp {
	
	public final Exp exp;

	public Neg(Exp exp) {
		this.exp = exp;
	}

	@Override
	public String unparse() {
		return "(- " + exp.unparse() + ")";
	}

	@Override
	public Exp obfuscate() {
		return new Neg(exp.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		exp.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return new Neg(exp.renameVariables(varMap));
	}

}
