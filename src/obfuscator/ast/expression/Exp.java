package obfuscator.ast.expression;

import obfuscator.VarMap;

/**
 * Abstract representation of Lua's expressions.
 */
public abstract class Exp {

	/**
	 * Return a string representation for the expression according to the syntax of Lua.
	 */
	abstract public String unparse();

	/**
	 * Return an obfuscated expression, whose interpretation remains equal to the current one.
	 */
	abstract public Exp obfuscate();
	
	/**
	 * Map every variable name on Exp with an obfuscated alias. 
	 */
    abstract public void getVariables(VarMap varMap);
	
    /**
     * Return the current Exp replacing every variable name with their alias.
     */
	abstract public Exp renameVariables(VarMap varMap);

}
