package obfuscator.ast.expression;

import obfuscator.ast.expression.Exp;
import obfuscator.ast.statement.FunctionCall;
import obfuscator.VarMap;

public class FunctionCallPrefix extends Exp{
	
	public final FunctionCall funcCall;
	
	public FunctionCallPrefix(FunctionCall funcCall) {
		this.funcCall = funcCall;
	}
	
	@Override
	public String unparse() {
		return funcCall.unparse();
	}

	@Override
	public Exp obfuscate() {
		return new FunctionCallPrefix(funcCall.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		funcCall.getVariables(varMap);
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		FunctionCall fc = funcCall.renameVariables(varMap);
		return new FunctionCallPrefix(fc);
	}

}
