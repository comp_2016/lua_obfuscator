package obfuscator.ast.expression;

import obfuscator.VarMap;

public class Ellipsis extends Exp {

	@Override
	public String unparse() {
		return "...";
	}

	@Override
	public Exp obfuscate() {
		return this;
	}

	@Override
	public void getVariables(VarMap varMap) {
		// NOOP
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return this;
	}

}
