package obfuscator.ast.expression;

import java.util.Random;
import obfuscator.ast.expression.Exp;
import obfuscator.ast.expression.binary_operator.*;
import obfuscator.VarMap;

public class Number extends Exp {
	
	public final Double number;

	public Number(Double number) {
		this.number = number;
	}

	@Override
	public String unparse() {
		return number.toString();
	}

	@Override
	public Exp obfuscate() {
		Random rnd = new Random();
		final int OPTIONS = (this.number == 0? 2 : 3); // If number == 0, then case 2 (module) is invalid
		int i = rnd.nextInt(OPTIONS);
		
		Number left, right;
		switch(i) {
			// Addition
			case 0:
				left = new Number(rnd.nextDouble() * this.number * -3);
				right = new Number(left.number * -1 + this.number);
				return new Addition(left, right);
			// Subtraction
			case 1:
				left = new Number(rnd.nextDouble() * this.number * 3);
				right = new Number(left.number - this.number);
				return new Subtraction(left, right);
			// Module
			case 2:
				right = new Number(this.number + rnd.nextDouble() * this.number);
				left = new Number(this.number + right.number * (1 + rnd.nextInt(3)));
				return new Module(left, right);
			default:
				throw new Error("Unexpected error at Number.obfuscate()!");
		}
	}

	@Override
	public void getVariables(VarMap varMap) {
		// NOOP
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return this;
	}
}
