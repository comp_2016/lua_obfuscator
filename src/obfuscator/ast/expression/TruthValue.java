package obfuscator.ast.expression;

import obfuscator.ast.expression.Exp;
import obfuscator.ast.expression.unary_operator.Not;
import obfuscator.VarMap;

public class TruthValue extends Exp {
	
	public final Boolean value;

	public TruthValue(Boolean value) {
		this.value = value;
	}

	@Override
	public String unparse() {
		return value ? "true" : "false";
	}

	@Override
	public Exp obfuscate() {
		if (value)
			return new Not(new Not(new LiteralString("FALSE!!"))); // return true
		else
			return new Not(new LiteralString("false")); // return false
	}

	@Override
	public void getVariables(VarMap varMap) {
		// NOOP
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return this;
	}
	
}
