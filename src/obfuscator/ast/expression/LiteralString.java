package obfuscator.ast.expression;

import obfuscator.ast.expression.Exp;
import obfuscator.VarMap;

public class LiteralString extends Exp {
	
	private final String str;

	public LiteralString(String str) {
		this.str = str;
	}
	
	@Override
	public String unparse() {
		return str;
	}

	@Override
	public Exp obfuscate() {
		String obfStr = "";
		char c;
		int ascciValue;
			
		obfStr += "string.char(";
		for (int i = 0; i < str.length(); i++) {
			c = str.charAt(i);
			ascciValue = (int) c;
				
			obfStr += ascciValue + (i == str.length()-1? "" : ",");
		}
		obfStr += ")";
		
		return new LiteralString(obfStr);
	}

	@Override
	public void getVariables(VarMap varMap) {
		// NOOP
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		return this;
	}

}
