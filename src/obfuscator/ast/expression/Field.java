package obfuscator.ast.expression;

import obfuscator.ast.expression.Exp;
import obfuscator.RandomString;
import obfuscator.VarMap;

public class Field extends Exp {

	public final Exp lExp;
	public final Exp rExp;
	public final String name;

	public Field(String name, Exp lExp, Exp rExp) {
		this.name = name;
		this.lExp = lExp;
		this.rExp = rExp;
	}

	@Override
	public String unparse() {
		String field = "";
		
		if (lExp != null && rExp != null) {
			field += "[ " + lExp.unparse() + " ] = " + rExp.unparse();
		} else if (name != null && rExp != null) {
			field += name + " = " + rExp.unparse();
		} else if (lExp != null) {
			field += lExp.unparse();
		}

		return field;
	}

	@Override
	public Exp obfuscate() {
		Exp field = null;
		
		if (name == null && lExp != null && rExp != null)
			field = new Field(null, lExp.obfuscate(), rExp.obfuscate());
		if (name != null && lExp == null && rExp != null)
			field = new Field(name, null, rExp.obfuscate());
		if (name == null && lExp != null && rExp == null) {
			field = new Field(null, lExp.obfuscate(), null);
		}
		
		return field;
	}

	@Override
	public void getVariables(VarMap varMap) {
		RandomString rndStr = new RandomString();
		
		if (lExp != null)
			lExp.getVariables(varMap);
		
		if (rExp != null)
			rExp.getVariables(varMap);
		
		if (name != null && VarMap.variables.get(name) == null) {
			String alias = rndStr.generateName();
			while (VarMap.variables.containsValue(alias))
				alias = rndStr.generateName();
			VarMap.variables.put(name, alias);
		}
	}

	@Override
	public Exp renameVariables(VarMap varMap) {
		Field field = this;
		String fieldName = (name != null? varMap.get(name) : null);
		
		if (lExp != null && rExp != null)
			field = new Field(fieldName, lExp.renameVariables(varMap), rExp.renameVariables(varMap));
		
		if (lExp == null && rExp != null)
			field = new Field(fieldName, null, rExp.renameVariables(varMap));
		
		if (lExp != null && rExp == null)
			field = new Field(fieldName, lExp.renameVariables(varMap), null);
		
		return field;
	}

}
