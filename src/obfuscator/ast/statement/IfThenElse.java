package obfuscator.ast.statement;

import java.util.ArrayList;
import obfuscator.ast.expression.Exp;
import obfuscator.ast.statement.Stat;
import obfuscator.VarMap;

public class IfThenElse extends Stat {

	public final ArrayList<Exp> conditions;
	public final ArrayList<Stat> thenBodies;
	public final Stat elseBody;

	public IfThenElse(IfThenList ifThenList, Stat elseBody) {
		this.conditions = ifThenList.conditions;
		this.thenBodies = ifThenList.thenBodies;
		this.elseBody = elseBody;
	}

	public IfThenElse(IfThenList ifThenList) {
		this.conditions = ifThenList.conditions;
		this.thenBodies = ifThenList.thenBodies;
		this.elseBody = null;
	}

	@Override
	public String unparse() {
		String result = "";
		
		for (int i = 0; i < conditions.size(); i++) {
			result += (i == 0? "if " : " elseif ");
			result += conditions.get(i).unparse() + " then " + thenBodies.get(i).unparse();
		}
		if (elseBody != null)
			result += " else " + elseBody.unparse();
		
		return result + " end";
	}

	@Override
	public Stat obfuscate() {
		IfThenList obfIfThenList = new IfThenList();
		
		ArrayList<Exp> obfConditions = new ArrayList<Exp>();
		for(Exp e : this.conditions) 
			obfConditions.add(e.obfuscate());
		obfIfThenList.conditions = obfConditions;
		
		ArrayList<Stat> obfThenBodies = new ArrayList<Stat>();
		for(Stat st : this.thenBodies) 
			obfThenBodies.add(st.obfuscate());
		obfIfThenList.thenBodies = obfThenBodies;
		
		if (elseBody == null)
			return new IfThenElse(obfIfThenList);
		else
			return new IfThenElse(obfIfThenList, elseBody.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		for(Exp e : conditions)
			e.getVariables(varMap);

		for(Stat s : thenBodies)
			s.getVariables(varMap);

		if(this.elseBody != null)
			elseBody.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		IfThenList itl = new IfThenList();
		
		ArrayList<Exp> c = new ArrayList<Exp>();
		for(Exp e : conditions)
			c.add(e.renameVariables(varMap));
		itl.conditions = c;
		
		ArrayList<Stat> tb = new ArrayList<Stat>();
		for(Stat s : thenBodies)
			tb.add(s.renameVariables(varMap));
		itl.thenBodies = tb;

		Stat e = this.elseBody;
		if(elseBody != null)
			e = elseBody.renameVariables(varMap);
		
		if (elseBody == null)
			return new IfThenElse(itl);
		else
			return new IfThenElse(itl, e);
	}
	
}
