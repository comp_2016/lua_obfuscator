package obfuscator.ast.statement;

import obfuscator.RandomString;
import obfuscator.VarMap;

public class Function extends Stat {

	public final Scope scope;
	public final String funcName;
	public final FuncBody funcBody;

	public Function(Scope scope, String funcName, FuncBody funcBody) {
		this.scope = scope;
		this.funcName = funcName;
		this.funcBody = funcBody;
	}

	@Override
	public String unparse() {
		String result = "function " + funcName;
		result += funcBody.unparse();

		switch (scope) {
			case GLOBAL:
				return result;
			case LOCAL:
				return "local " + result;
			default:
				return null;
		}
	}

	@Override
	public Stat obfuscate() {
		return new Function(scope, funcName, funcBody.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		RandomString rndStr = new RandomString();
		
		if (VarMap.variables.get(funcName) == null) {
			String alias = rndStr.generateName();
			while (VarMap.variables.containsValue(alias))
				alias = rndStr.generateName();
			VarMap.variables.put(funcName, alias);
		}
		
		funcBody.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		String fn = funcName; 
		if (VarMap.variables.containsKey(funcName))
			fn = VarMap.variables.get(funcName);
		
		FuncBody fb = (FuncBody) funcBody.renameVariables(varMap);
		
		return new Function(this.scope, fn, fb);
	}

}
