package obfuscator.ast.statement;

import obfuscator.ast.expression.Exp;
import obfuscator.ast.statement.Stat;
import obfuscator.VarMap;

public class Repeat extends Stat {

	public final Stat body;
	public final Exp condition;

	public Repeat(Stat body, Exp condition) {
		this.body = body;
		this.condition = condition;
	}

	@Override
	public String unparse() {
		return "repeat " + body.unparse() + " until " + condition.unparse();
	}

	@Override
	public Stat obfuscate() {
		return new Repeat (body.obfuscate(), condition.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		body.getVariables(varMap);
		condition.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap statVar) {
		Stat b = body.renameVariables(statVar);
		Exp c = condition.renameVariables(statVar);
		
		return new Repeat(b, c);	
	}
	
}
