package obfuscator.ast.statement;

import obfuscator.ast.expression.Exp;
import obfuscator.ast.statement.Stat;
import obfuscator.VarMap;

public class While extends Stat {
	
	public final Exp condition;
	public final Stat body;

	public While(Exp condition, Stat body) {
		this.condition = condition;
		this.body = body;
	}

	@Override
	public String unparse() {
		return "while " + condition.unparse() + " do " + body.unparse() + " end";
	}

	@Override
	public Stat obfuscate() {
		return new While(condition.obfuscate(), body.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		condition.getVariables(varMap);
		body.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		Exp c = condition.renameVariables(varMap);
		Stat b = body.renameVariables(varMap);
		
		return new While(c, b);
	}
	
}
