package obfuscator.ast.statement;

public enum Scope {
	GLOBAL, LOCAL
}
