package obfuscator.ast.statement;

import java.util.ArrayList;
import java.util.Random;
import obfuscator.RandomString;
import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;
import obfuscator.ast.expression.Number;
import obfuscator.ast.expression.TruthValue;
import obfuscator.ast.expression.Variable;
import obfuscator.ast.expression.binary_operator.Addition;

public class Block extends Stat {
	public final ArrayList<Stat> statements;
	
	public Block(ArrayList<Stat> statements) {
		this.statements = statements;
	}

	@Override
	public String unparse() {
		Random rnd = new Random();
		RandomString rndStr = new RandomString();
		String result = "";
		
		ArrayList<String> comment;
		for (Stat statement : statements) {
			if (rnd.nextInt(5) == 0) {
				comment = rndStr.generateComment();
				if (comment.size() == 1)
					result += "--" + comment.get(0);
				else {
					result += "--[[";
					for (String line : comment)
						result += line + "\n";
					result += "]]";
				}		
			}
			result += statement.unparse() + "\n ";
		}
		return result;
	}

	@Override
	public Block obfuscate() {
		ArrayList<Stat> obfuscatedStatements = new ArrayList<Stat>();
		for (Stat s : statements)
			obfuscatedStatements.add(s.obfuscate());
		
		Random rnd = new Random();
		RandomString rndStr = new RandomString();
		final int OPTIONS = 5;
		int i, rndIndex;
		Stat deadStat;
		
		for(int j = 0; j <= 1 + rnd.nextInt(statements.size()/2); i++) {
			i = rnd.nextInt(OPTIONS);
			
			switch(i) {
				case 0:
					deadStat = generateDo(rnd, rndStr);
					break;
				case 1:
					deadStat = generateFor(rnd, rndStr);
					break;
				case 2:
					deadStat = generateIfThenElse(rnd, rndStr);
					break;
				case 3:
					deadStat = generateRepeat(rnd, rndStr);
					break;
				case 4:
					deadStat = generateWhile(rnd, rndStr);
					break;
				default:
					deadStat = null;
					break;
			}
			
			rndIndex = rnd.nextInt(obfuscatedStatements.size());
			if (deadStat != null)
				obfuscatedStatements.add(rndIndex, deadStat.obfuscate());
		}	
		
		return new Block(obfuscatedStatements);
	}

	@Override
	public void getVariables(VarMap varMap) {
		for (Stat stat : statements)
			stat.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		ArrayList<Stat> s = new ArrayList<Stat>();
		for (Stat stat : statements)
			s.add(stat.renameVariables(varMap));

		return new Block(s);
	}
	
	private Stat generateDo(Random rnd, RandomString rndStr) {
		ArrayList<Stat> s = new ArrayList<Stat>();
		
		ArrayList<Variable> varList = new ArrayList<Variable>();
		Variable var1 = new Variable(rndStr.generateName());
		varList.add(var1);
		Variable var2 = new Variable(rndStr.generateName());
		varList.add(var2);
		
		ArrayList<Exp> expList = new ArrayList<Exp>();
		expList.add(new Number(rnd.nextDouble() * rnd.nextInt(1337)));
		expList.add(new Number(rnd.nextDouble() * rnd.nextInt(404)));
		
		s.add(new Assignment(Scope.GLOBAL, varList, expList, true));
		expList.set(1, new Addition(var1, var2));
		s.add(new Assignment(Scope.GLOBAL, varList, expList, true));
		
		Stat block = new Block(s);

		return new Do(block);
	}
	
	private Stat generateFor(Random rnd, RandomString rndStr) {
		ArrayList<Exp> expList = new ArrayList<Exp>();
		expList.add(new Number(1.0));
		expList.add(new Number(1.0));
		expList.add(new Number(2.0));
		expList.add(new Number(3.0));
		expList.add(new Number(5.0));
		expList.add(new Number(8.0));
		expList.add(new Number(13.0));
		expList.add(new Number(21.0));
		
		return new NumericFor(rndStr.generateName(), expList, generateDo(rnd, rndStr));
	}
	
	private Stat generateIfThenElse(Random rnd, RandomString rndStr) {
		Exp condition = new TruthValue(false);
		
		IfThenList itl = new IfThenList();
		itl.add(condition, generateDo(rnd, rndStr));
		
		return new IfThenElse(itl);
	}
	
	private Stat generateRepeat(Random rnd, RandomString rndStr) {
		return new Repeat(generateFor(rnd, rndStr), new TruthValue(false));
	}
	
	private Stat generateWhile(Random rnd, RandomString rndStr) {
		return new While(new TruthValue(false), generateRepeat(rnd, rndStr));
	}
}
