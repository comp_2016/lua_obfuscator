package obfuscator.ast.statement;

import java.util.ArrayList;
import obfuscator.ast.expression.Exp;
import obfuscator.ast.statement.Stat;
import obfuscator.RandomString;
import obfuscator.VarMap;

public class NumericFor extends Stat {

	public final String name;
	public final ArrayList<Exp> expList;
	public final Stat body;

	public NumericFor(String name, ArrayList<Exp> expList, Stat body) {
		this.name = name;
		this.expList = expList;
		this.body = body;
	}

	@Override
	public String unparse() {
		String result = "for " + name + " = ";
		for (int i = 0; i < expList.size(); i++) {
			result += ", " + expList.get(i).unparse();
			result += (i == expList.size()-1? "" : ", ");
		}
		result += " do " + body.unparse();
		return result + " end";
	}

	@Override
	public Stat obfuscate() {
		ArrayList<Exp> obfExpList = new ArrayList<Exp>();
		for(Exp e : expList)
			obfExpList.add(e.obfuscate());
		
		return new NumericFor(name, obfExpList, body.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		RandomString rndStr = new RandomString();
		
		if (VarMap.variables.get(name) == null) {
			String alias = rndStr.generateName();
			while (VarMap.variables.containsValue(alias))
				alias = rndStr.generateName();
			VarMap.variables.put(name, alias);
		}
		
		for(Exp e : expList)
			e.getVariables(varMap);
		
		body.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		String n = this.name; 
		if (VarMap.variables.get(name) != null)
			n = varMap.get(name);
		
		ArrayList<Exp> el = new ArrayList<Exp>(); 
		for(Exp e : expList)
			el.add(e.renameVariables(varMap));
		
		Stat b = body.renameVariables(varMap);
		
		return new NumericFor(n, el, b);
	}
	
}
