package obfuscator.ast.statement;

import obfuscator.ast.statement.Stat;
import obfuscator.VarMap;

public class Do extends Stat {
	
	public final Stat body;

	public Do(Stat body) {
		this.body = body;
	}

	@Override
	public String unparse() {
		return "do " + body.unparse() + " end";
	}

	@Override
	public Stat obfuscate() {
		return new Do(body.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		body.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		return new Do(body.renameVariables(varMap));
	}
	
}
