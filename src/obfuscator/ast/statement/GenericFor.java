package obfuscator.ast.statement;

import java.util.ArrayList;
import obfuscator.ast.expression.Exp;
import obfuscator.ast.statement.Stat;
import obfuscator.RandomString;
import obfuscator.VarMap;

public class GenericFor extends Stat {
	public final ArrayList<String> nameList;
	public final ArrayList<Exp> expList;
	public final Stat body;

	public GenericFor(ArrayList<String> nameList, ArrayList<Exp> expList, Stat body) {
		this.nameList = nameList;
		this.expList = expList;
		this.body = body;
	}

	@Override
	public String unparse() {
		String result = "for ";
		for (int i = 0; i < nameList.size(); i++) {
			result += nameList.get(i);
			result += (i == nameList.size()-1? "" : ", ");
		}
		result += " in ";
		for (int i = 0; i < expList.size(); i++) {
			result += expList.get(i).unparse();
			result += (i == expList.size()-1? "" : ", ");
		}
		result += " do " + body.unparse();
		return result + " end";
	}

	@Override
	public Stat obfuscate() {
		ArrayList<Exp> obfExpList = new ArrayList<Exp>();
		for(Exp e : expList)
			obfExpList.add(e.obfuscate());
		
		return new GenericFor(nameList, obfExpList, body.obfuscate());
	}
	
	@Override
	public void getVariables(VarMap varMap) {
		RandomString rndStr = new RandomString();
		
		for(String name : nameList)
			if (VarMap.variables.get(name) == null) {
				String alias = rndStr.generateName();
				while (VarMap.variables.containsValue(alias))
					alias = rndStr.generateName();
				VarMap.variables.put(name, alias);
			}
	
		for(Exp e : expList)
			e.getVariables(varMap);
		
		body.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		ArrayList<String> nl = new ArrayList<String>();		
		for(String name : nameList) 
			if (VarMap.variables.get(name) != null)
				nl.add(varMap.get(name));
		
		ArrayList<Exp> el = new ArrayList<Exp>();
		for(Exp e : expList) 
			el.add(e.renameVariables(varMap));
		
		Stat b = body.renameVariables(varMap);
		
		return new GenericFor(nl, el, b);
	}
	
}
