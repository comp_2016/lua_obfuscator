package obfuscator.ast.statement;

import java.util.ArrayList;
import obfuscator.ast.expression.Exp;
import obfuscator.RandomString;
import obfuscator.VarMap;

public class FunctionCall extends Stat {
	public final Exp prefix;
	public final String name;
	public final ArrayList<Exp> args;

	public FunctionCall(Exp prefix, String name, ArrayList<Exp> args) {
		this.prefix = prefix;
		this.name = name;
		this.args = args;
	}

	public String unparse() {
		String result = "";
		
		result += prefix.unparse();
		if (this.name != null)
			result += " : " + name;
		
		result += " (";
		for (int i = 0; i < args.size(); i++) {
			result += args.get(i).unparse();
			result += (i == args.size()-1? "" : ", ");
		}
		result += ")";
		
		return result;
	}

	public FunctionCall obfuscate() {
		Exp obfPrefix = prefix.obfuscate();
		ArrayList<Exp> obfArgs = new ArrayList<Exp>();
		
		for (Exp e : args)
			obfArgs.add(e.obfuscate());

		return new FunctionCall(obfPrefix, name, obfArgs);
	}

	public void getVariables(VarMap varMap) {
		RandomString rndStr = new RandomString();
		
		prefix.getVariables(varMap);
		
		if (name != null && VarMap.variables.get(name) == null) {
			String alias = rndStr.generateName();
			while (VarMap.variables.containsValue(alias))
				alias = rndStr.generateName();
			VarMap.variables.put(name, alias);
		}
		for (Exp e : args)
			e.getVariables(varMap);
	}

	public FunctionCall renameVariables(VarMap varMap) {
		String n = varMap.get(name);
		ArrayList<Exp> a = new ArrayList<Exp>();
		
		for (Exp e : args)
			a.add(e.renameVariables(varMap));

		if (n != null)
			return new FunctionCall(prefix.renameVariables(varMap), n, a);
		else
			return new FunctionCall(prefix.renameVariables(varMap), name, a);
	}

}
