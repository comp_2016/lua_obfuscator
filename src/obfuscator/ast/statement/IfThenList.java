package obfuscator.ast.statement;

import java.util.ArrayList;
import obfuscator.ast.expression.Exp;
import obfuscator.ast.statement.Stat;
import obfuscator.VarMap;

/**
 * Auxiliary structure for IfThenElse statements.
 */
public class IfThenList extends Stat{

	public ArrayList<Exp> conditions;
	public ArrayList<Stat> thenBodies;

	public void add(Exp condition, Stat thenBody) {
		conditions.add(condition);
		thenBodies.add(thenBody);
	}

	public IfThenList() {
		this.conditions = new ArrayList<Exp>();
		this.thenBodies = new ArrayList<Stat>();
	}
	
	@Override
	public String unparse() {
		return null;
	}

	@Override
	public Stat obfuscate() {
		return this;
	}

	@Override
	public void getVariables(VarMap varMap) {
		for(Exp e : conditions)
			e.getVariables(varMap);
		
		for(Stat s : thenBodies)
			s.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		IfThenList itl = new IfThenList();
		
		ArrayList<Exp> c = new ArrayList<Exp>();
		for(Exp e : conditions)
			c.add(e.renameVariables(varMap));
		itl.conditions = c;
		
		ArrayList<Stat> tb = new ArrayList<Stat>();
		for(Stat s : thenBodies)
			tb.add(s.renameVariables(varMap));		
		itl.thenBodies = tb;
		
		return itl;
	}

}
