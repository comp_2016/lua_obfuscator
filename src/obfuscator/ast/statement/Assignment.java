package obfuscator.ast.statement;

import java.util.ArrayList;
import obfuscator.ast.expression.Exp;
import obfuscator.ast.statement.Stat;
import obfuscator.ast.expression.Variable;
import obfuscator.RandomString;
import obfuscator.VarMap;

public class Assignment extends Stat {
	
	public final Scope scope;
	public final ArrayList<Variable> varList;
	public final ArrayList<String> nameList;
	public final ArrayList<Exp> expList;

	public Assignment(Scope scope, ArrayList<String> nameList, ArrayList<Exp> expList) {
		this.scope = scope;
		this.varList = null;
		this.nameList = nameList;
		this.expList = expList;
	}

	public Assignment(Scope scope, ArrayList<Variable> varList, ArrayList<Exp> expList, Boolean isMul) {
		this.scope = scope;
		this.varList = varList;
		this.nameList = null;
		this.expList = expList;
	}

	public Assignment(ArrayList<String> nameList) {
		this.scope = Scope.LOCAL;
		this.varList = null;
		this.nameList = nameList;
		this.expList = null;
	}

	@Override
	public String unparse() {
		String assignment = "";
		
		if (varList != null && varList.size() > 0) {
			for (int i = 0; i < varList.size(); i++) {
				assignment += varList.get(i).unparse();
				assignment += (i == varList.size()-1? "" : ", ");
			}
		} else {
			for (int i = 0; i < nameList.size(); i++) {
				assignment += nameList.get(i);
				assignment += (i == nameList.size()-1? "" : ", ");
			}
		}
		
		if (expList != null && expList.size() > 0) {
			assignment += " = ";
			for (int i = 0; i < expList.size(); i++) {
				assignment += expList.get(i);
				assignment += (i == expList.size()-1? "" : ", "); 
			}
		}
		
		switch (scope) {
			case GLOBAL:
				return assignment;
			case LOCAL:
				return "local " + assignment;
			default:
				return null;
		}
	}

	@Override
	public Stat obfuscate() {
		Assignment result = this;
		
		ArrayList<Variable> obfVarList = new ArrayList<Variable>();
		ArrayList<Exp> obfExpList = new ArrayList<Exp>();
		
		if (varList != null && varList.size() > 0) {
			for (int i = 0; i < varList.size(); i++)
				obfVarList.add((Variable) varList.get(i).obfuscate());
		}
		
		if (expList != null && expList.size() > 0) {
			for (int i = 0; i < expList.size(); i++)
				obfExpList.add(expList.get(i).obfuscate());
		}
		
		if (scope != null && nameList != null && this.expList != null)
			result = new Assignment(scope, nameList, obfExpList);
		else if (scope != null && varList != null && expList != null)
			result = new Assignment(this.scope, obfVarList, obfExpList, true);
		else if (varList == null && nameList != null)
			result = new Assignment(nameList);
		
		return result;
	}

	@Override
	public void getVariables(VarMap varMap) {
		RandomString rndStr = new RandomString();
		
		if (this.nameList != null) {
			for (String name : nameList) {
				if (VarMap.variables.get(name) == null) {
					String alias = rndStr.generateName();
					while (VarMap.variables.containsValue(alias))
						alias = rndStr.generateName();
					VarMap.variables.put(name, alias);
				}
			}
		}
		
		if (varList != null) {
			for (Variable v : varList)
				v.getVariables(varMap);
		}
		
		if (expList != null) {
			for (Exp e : expList)
				e.getVariables(varMap);
		}
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		Assignment result = this;
		
		ArrayList<Variable> vl = new ArrayList<Variable>();
		ArrayList<String> nl = new ArrayList<String>();
		ArrayList<Exp> el = new ArrayList<Exp>();
		
		if (nameList != null) {
			for (String name : nameList) {
				if (VarMap.variables.get(name) != null)
					nl.add(VarMap.variables.get(name));
			}
		}
		
		if (varList != null) {
			for (Variable v : varList)
				vl.add((Variable) v.renameVariables(varMap));
		}
		
		if (expList != null) {
			for (Exp e : expList)
				el.add(e.renameVariables(varMap));
		}
		
		if (scope != null && nameList != null && expList != null)
			result = new Assignment(scope, nl, el);
		else if (scope != null && varList != null && expList != null)
			result = new Assignment(scope, vl, el, true);
		else if (varList == null && nameList != null)
			result = new Assignment(nl);
		
		return result;
	}
	
}
