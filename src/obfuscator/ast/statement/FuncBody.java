package obfuscator.ast.statement;

import java.util.ArrayList;
import obfuscator.RandomString;
import obfuscator.VarMap;

public class FuncBody extends Stat {
	
	public final ArrayList<String> parList;
	public final Block block;

	public FuncBody(ArrayList<String> parList, Block block) {
		this.parList = parList;
		this.block = block;
	}

	@Override
	public String unparse() {
		String result = "(";
		for (int i = 0; i < parList.size(); i++) {
			result += parList.get(i);
			result += (i == parList.size()-1? "" : ", ");
		}
		result += ") ";
		
		if (this.block != null)
			result += block.unparse();
		
		return result + " end";
	}

	@Override
	public FuncBody obfuscate() {
		return new FuncBody(parList, block.obfuscate());
	}

	@Override
	public void getVariables(VarMap varMap) {
		RandomString rndStr = new RandomString();
		
		for(String name : parList){
			if (VarMap.variables.get(name) == null) {
				String alias = rndStr.generateName();
				while (VarMap.variables.containsValue(alias))
					alias = rndStr.generateName();
				VarMap.variables.put(name, alias);
			}
		}
		block.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		ArrayList<String> pl = new ArrayList<String>();
		for(String name : parList){
			if (VarMap.variables.get(name) != null)
				pl.add(varMap.get(name));
		}
		
		Block b = (Block) block.renameVariables(varMap);
		
		return new FuncBody(pl, b);
	}

}
