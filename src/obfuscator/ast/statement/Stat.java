package obfuscator.ast.statement;

import obfuscator.VarMap;

/**
 * Abstract representation of Lua's statements.
 */
public abstract class Stat {

	/**
	 * Return a string representation for the statement according to the syntax of Lua.
	 */
	abstract public String unparse();

	/**
	 * Return an obfuscated statement, whose interpretation remains equal to the current one.
	 */
	abstract public Stat obfuscate();
	
	/**
	 * Map every variable name on Stat with an obfuscated alias. 
	 */
	abstract public void getVariables(VarMap statVar);
	
	/**
     * Return the current Stat replacing every variable name with their alias.
     */
	abstract public Stat renameVariables(VarMap statVar);

}
