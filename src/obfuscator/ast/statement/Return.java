package obfuscator.ast.statement;

import java.util.ArrayList;
import obfuscator.VarMap;
import obfuscator.ast.expression.Exp;

public class Return extends Stat {
	public final ArrayList<Exp> expList;
	public final String strBreak;

	public Return() {
		this.expList = null;
		this.strBreak = null;
	}
	
	public Return(ArrayList<Exp> expList) {
		this.expList = expList;
		this.strBreak = null;
	}

	public Return(String strBreak) {
		this.expList = null;
		this.strBreak = strBreak;
	}

	@Override
	public String unparse() {
		if (expList == null && strBreak == null)
			return "return";
		else if (expList != null) {
			String result = "return ";
			for (int i = 0; i < expList.size(); i++) {
				result += expList.get(i).unparse();
				result += (i == expList.size()-1? "" : ", ");
			}
			return result;
		} else if (strBreak != null)
			return "break";
		return "";
	}

	@Override
	public Stat obfuscate() {
		if (expList == null && strBreak == null)
			return new Return();
		else if (expList != null) {
			ArrayList<Exp> obfExpList = new ArrayList<Exp>();
			for (int i = 0; i < expList.size(); i++)
				obfExpList.add(expList.get(i).obfuscate());
			return new Return(obfExpList);
		} else if (strBreak != null)
			return new Return(strBreak);
		return this;
	}

	@Override
	public void getVariables(VarMap varMap) {
		if (this.expList != null)
			for (Exp e : expList)
				e.getVariables(varMap);
	}

	@Override
	public Stat renameVariables(VarMap varMap) {
		if (expList == null && strBreak == null)
			return new Return();
		else if (expList != null) {
			ArrayList<Exp> el = new ArrayList<Exp>();
			for (int i = 0; i < expList.size(); i++)
				el.add(expList.get(i).renameVariables(varMap));
			return new Return(el);
		} else if (strBreak != null)
			return new Return(strBreak);
		return this;
	}

}
