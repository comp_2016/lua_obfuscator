package obfuscator;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import obfuscator.ast.statement.Block;
import parser.*;

public class Main {

	public static void main(String[] args) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		StringBuilder source = new StringBuilder();
        
		for (String line; (line = in.readLine()) != null && line.length() > 0; /* NOOP */) {
			source.append(line + "\n");
		}
		
		VarMap varMap = new VarMap();
		
		try {
			Block code = (Block) Parser.parse(source.toString()).value;
			System.out.println("// source = \n//\t" + source.toString().replace("\n", "\n//\t"));
			
			System.out.println(" Obfuscated code: \n");
			Block obfCode = (Block) code.obfuscate();
			System.out.println(obfCode.unparse());
			
			/*obfCode.getVariables(varMap);
			System.out.println("\n Variable mapping: \n");
			System.out.println(varMap.variables.keySet());
			System.out.println(varMap.variables.values() + "\n");*/
			
			System.out.println("\n Variable renaming: \n");
			obfCode = (Block) obfCode.renameVariables(varMap);
			System.out.println(obfCode.unparse());
			
			System.out.println("\n Probando a ver si compila despues de todo el proceso \n");
			Block codeTest = (Block) Parser.parse(obfCode.unparse()).value;
			System.out.println(codeTest.unparse());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}