package parser;

import java_cup.runtime.Symbol;
import java.util.*;
import java.io.*;

%%

%unicode
%line
%column
%class Lexer
%cupsym Tokens
%cup
%implements Tokens

%{:

	public static List<Symbol> tokens(Reader input) throws IOException {
		Lexer lexer = new Lexer(input);
		List<Symbol> result = new ArrayList<Symbol>();
		for (Symbol token = lexer.next_token(); token.sym != Tokens.EOF; token = lexer.next_token()) {
			result.add(token);
		}
		return result;
	}

	public static void main(String[] args) throws IOException {
		Lexer lexer;
		if (args.length < 1) args = new String[]{ "" };
		for (int i = 0; i < args.length; ++i) {
			lexer = new Lexer(new InputStreamReader(args[i].length() > 0 ? new FileInputStream(args[i]) : System.in, "UTF8"));
			System.out.println(args[i] +":");
			for (Symbol token = lexer.next_token(); token.sym != Tokens.EOF; token = lexer.next_token()) {
				System.out.println("\t#"+ token.sym +" "+ token.value);
			}
		}
	}
%}

%%
"while"
	{ return new Symbol(WHILE, yyline, yycolumn, yytext()); }
"do"
	{ return new Symbol(DO, yyline, yycolumn, yytext()); }
"end"
	{ return new Symbol(END, yyline, yycolumn, yytext()); }
"repeat"
	{ return new Symbol(REPEAT, yyline, yycolumn, yytext()); }
"until"
	{ return new Symbol(UNTIL, yyline, yycolumn, yytext()); }
"if"
	{ return new Symbol(IF, yyline, yycolumn, yytext()); }
"then"
	{ return new Symbol(THEN, yyline, yycolumn, yytext()); }
"elseif"
	{ return new Symbol(ELSEIF, yyline, yycolumn, yytext()); }
"else"
	{ return new Symbol(ELSE, yyline, yycolumn, yytext()); }
"for"
	{ return new Symbol(FOR, yyline, yycolumn, yytext()); }
"in"
	{ return new Symbol(IN, yyline, yycolumn, yytext()); }
"function"
	{ return new Symbol(FUNCTIONWORD, yyline, yycolumn, yytext()); }
"local"
	{ return new Symbol(LOCAL, yyline, yycolumn, yytext()); }
"return"
	{ return new Symbol(RETURN, yyline, yycolumn, yytext()); }
"break"
	{ return new Symbol(BREAK, yyline, yycolumn, yytext()); }
"nil"
	{ return new Symbol(NIL, yyline, yycolumn, yytext()); }
"false"
	{ return new Symbol(FALSE, yyline, yycolumn, yytext()); }
"true"
	{ return new Symbol(TRUE, yyline, yycolumn, yytext()); }
-?(0|[1-9][0-9]*)(\.[0-9]+)?([eE][+-]?[0-9]+)?
	{ String $1 = yytext(); Double $0 = Double.parseDouble($1);
	  return new Symbol(NUMBER, yyline, yycolumn, $0); }
":"
	{ return new Symbol(COLON, yyline, yycolumn, yytext()); }
";"
	{ return new Symbol(SEMICOLON, yyline, yycolumn, yytext()); }
","
	{ return new Symbol(COMMA, yyline, yycolumn, yytext()); }
"."
	{ return new Symbol(PERIOD, yyline, yycolumn, yytext()); }
".."
	{ return new Symbol(TWO_PERIODS, yyline, yycolumn, yytext()); }
"..."
	{ return new Symbol(ELLIPSIS, yyline, yycolumn, yytext()); }
"["
	{ return new Symbol(LEFT_BRACKET, yyline, yycolumn, yytext()); }
"]"
	{ return new Symbol(RIGHT_BRACKET, yyline, yycolumn, yytext()); }
"("
	{ return new Symbol(LEFT_ROUND_BRACKET, yyline, yycolumn, yytext()); }
")"
	{ return new Symbol(RIGHT_ROUND_BRACKET, yyline, yycolumn, yytext()); }
"{"
	{ return new Symbol(LEFT_CURLY_BRACKET, yyline, yycolumn, yytext()); }
"}"
	{ return new Symbol(RIGHT_CURLY_BRACKET, yyline, yycolumn, yytext()); }
"="
	{ return new Symbol(EQUALS_SIGN, yyline, yycolumn, yytext()); }
"+"
	{ return new Symbol(PLUS_SIGN, yyline, yycolumn, yytext()); }
"-"
	{ return new Symbol(HYPHEN_MINUS, yyline, yycolumn, yytext()); }
"*"
	{ return new Symbol(ASTERISK, yyline, yycolumn, yytext()); }
"/"
	{ return new Symbol(SLASH, yyline, yycolumn, yytext()); }
"^"
	{ return new Symbol(CARET, yyline, yycolumn, yytext()); }
"%"
	{ return new Symbol(PERCENT_SIGN, yyline, yycolumn, yytext()); }
"<"
	{ return new Symbol(LESS_THAN, yyline, yycolumn, yytext()); }
"<="
	{ return new Symbol(LESS_THAN_OR_EQUAL, yyline, yycolumn, yytext()); }
">"
	{ return new Symbol(GREATER_THAN, yyline, yycolumn, yytext()); }
">="
	{ return new Symbol(GREATER_THAN_OR_EQUAL, yyline, yycolumn, yytext()); }
"=="
	{ return new Symbol(DOUBLE_EQUALS_SIGN, yyline, yycolumn, yytext()); }
"~="
	{ return new Symbol(DASH_EQUALS_SIGN, yyline, yycolumn, yytext()); }
"and"
	{ return new Symbol(AND, yyline, yycolumn, yytext()); }
"or"
	{ return new Symbol(OR, yyline, yycolumn, yytext()); }
"not"
	{ return new Symbol(NOT, yyline, yycolumn, yytext()); }
"#"
	{ return new Symbol(NUMBER_SIGN, yyline, yycolumn, yytext()); }
[a-zA-Z_][a-zA-Z0-9_]*
	{ return new Symbol(NAME, yyline, yycolumn, yytext()); }
\"([^\"\\\n]| \\[bntrf\"\\/]|\\u[0-9a-fA-F]{4})*\"
	{ return new Symbol(STRING, yyline, yycolumn, yytext()); }
[ \t\r\n\f\v]+
	{ /* Ignore */ }
 //\/\*+([^\*]|\*+[^\/])*\*+\/
 --\[+([^\-\[]|--+[^\[])*--+\]
	{ /* Ignore */ }
--[^\n]*\n
	{ /* Ignore */ }
.
	{ return new Symbol(error, yyline, yycolumn, "Unexpected input <"+ yytext() +">!"); }