
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Sun Nov 20 12:57:45 UYT 2016
//----------------------------------------------------

package parser;

/** CUP generated class containing symbol constants. */
public class Tokens {
  /* terminals */
  public static final int RIGHT_BRACKET = 24;
  public static final int ASTERISK = 32;
  public static final int SLASH = 33;
  public static final int PERIOD = 20;
  public static final int LESS_THAN_OR_EQUAL = 37;
  public static final int DASH_EQUALS_SIGN = 41;
  public static final int REPEAT = 5;
  public static final int FOR = 11;
  public static final int GREATER_THAN = 38;
  public static final int NOT = 44;
  public static final int AND = 42;
  public static final int SEMICOLON = 18;
  public static final int ELSEIF = 10;
  public static final int NIL = 47;
  public static final int OR = 43;
  public static final int IN = 12;
  public static final int ELLIPSIS = 22;
  public static final int COMMA = 19;
  public static final int PERCENT_SIGN = 35;
  public static final int UNTIL = 6;
  public static final int LEFT_CURLY_BRACKET = 27;
  public static final int IF = 7;
  public static final int CARET = 34;
  public static final int LEFT_ROUND_BRACKET = 25;
  public static final int EOF = 0;
  public static final int RETURN = 15;
  public static final int TRUE = 49;
  public static final int error = 1;
  public static final int NUMBER = 50;
  public static final int BREAK = 16;
  public static final int NAME = 46;
  public static final int COLON = 17;
  public static final int PLUS_SIGN = 30;
  public static final int ELSE = 9;
  public static final int WHILE = 2;
  public static final int NUMBER_SIGN = 45;
  public static final int THEN = 8;
  public static final int HYPHEN_MINUS = 31;
  public static final int FUNCTIONWORD = 13;
  public static final int END = 4;
  public static final int STRING = 51;
  public static final int LOCAL = 14;
  public static final int GREATER_THAN_OR_EQUAL = 39;
  public static final int EQUALS_SIGN = 29;
  public static final int FALSE = 48;
  public static final int LESS_THAN = 36;
  public static final int RIGHT_CURLY_BRACKET = 28;
  public static final int LEFT_BRACKET = 23;
  public static final int TWO_PERIODS = 21;
  public static final int DO = 3;
  public static final int RIGHT_ROUND_BRACKET = 26;
  public static final int DOUBLE_EQUALS_SIGN = 40;
}

